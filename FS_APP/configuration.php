<?php

/**
 *
 *      PHP is an Opensource App configuration configs
 *
 * 	@author Samuel da Silva Mendes
 *
 * 	Copyright (c) 2018 "App configuration configs"
 *
 * 	This file is part App configuration configs.
 *
 * 	URA App Asterisk is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * 	GNU General Public License for more details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program. If not, see <http://www.gnu.org/licenses/>
 */
use App;

require ("config.php");

/**
 * Carregar os objetos da classe Modules()
 */
$modulo = new App\Lib\FS\Modules($_POST);

/**
 * Carregar os objetos da classe XmlCurl()
 */
$xml = new App\Lib\FS\XmlCurl();

/**
 * Carregar os objetos da classe Conexao()
 */
$db = new App\Db\Conexao();

/**
 * Carregar objeto conexao da classe Conexao()
 */
$conn = $db->getConn();

/**
 * Carregar os objetos da classe Modal()
 */
$sql = new App\Db\Modal();

/**
 * Retornao objeto com o valor do retorno dos objetos Modulos.
 */
$result = $modulo->getModules();

switch ($result) {
    case "spandsp.conf":
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case "enum.conf":
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'timezones.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'local_stream.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'event_socket.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'acl.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'post_load_switch.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'post_load_modules.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'lua.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'shout.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'opus.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'httapi.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'voicemail.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'hash.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'fifo.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'db.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'verto.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'sofia.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'conference.conf':
        /**
         * Retornando objeto not found no padrão correto do freeswitch. 
         */
        $xml->setNULL();
        print_r($xml->getXML());
        break;
    case 'format_cdr.conf':
        try {
            /**
             * Carregar a visão dos Modulos
             */
            $res = $sql->vw_modules_format_cdr($_POST['hostname']);
            /**
             * Carregar os objetos da classe Format_cdr()
             */
            $format = new \App\Lib\FS\Format_cdr();
            if (!empty($res)) {
                $format->format($res);
            } else {
                /**
                 * Retornando objeto not found no padrão correto do freeswitch. 
                 */
                $xml->setNULL();
                print_r($xml->getXML());
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        break;
    case 'ivr.conf':
        try {
            $ivr = new App\Lib\FS\Ivr();
            $fs = $sql->getFsIvr($_POST['hostname'], $result);
            /**
             * Carregar os objetos da calsse Ivr()
             */
            if (!empty($fs[0]->id)) {
                $ivr->getIvrMenu($fs[0]);
            } else {
                /**
                 * Retornando objeto not found no padrão correto do freeswitch. 
                 */
                $xml->setNULL();
                print_r($xml->getXML());
            }
        } catch (Exception $exc) {
            print_r($exc->getTraceAsString());
        }

        break;
    default :
        try {
            $res = $sql->Module($result);
            if (!empty($res)) {
                $modulo->setModules($res);
            } else {
                /**
                 * Retornando objeto not found no padrão correto do freeswitch. 
                 */
                $xml->setNULL();
                print_t($xml->getXML());
            }
        } catch (Exception $exc) {
            print_r($exc->getTraceAsString());
        }
        break;
}
