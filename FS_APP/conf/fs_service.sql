DROP TABLE IF EXISTS modules;

CREATE TABLE modules (
	id SERIAL NOT NULL,
	type VARCHAR(80),
	name VARCHAR(120),
	description VARCHAR(255),
	fs VARCHAR(120),
	fs_key VARCHAR(120),
	PRIMARY KEY (id),
	UNIQUE (id)
);

DROP TABLE IF EXISTS modules_callcenter;

CREATE TABLE modules_callcenter (
	id SERIAL NOT NULL,
	name VARCHAR(120),
	PRIMARY KEY (id),
	fk_id_host INTEGER REFERENCES modules (id) ON DELETE CASCADE
);


DROP TABLE IF EXISTS modules_callcenter_setting;

CREATE TABLE modules_callcenter_setting (
	id SERIAL NOT NULL,
	db INTEGER NOT NULL DEFAULT 0,
	dsn VARCHAR(120) NOT NULL DEFAULT 'odbc-dsn',
	driver VARCHAR(256) NOT NULL DEFAULT 'pgsql::',
	PRIMARY KEY (id),
	fk_id_host INTEGER REFERENCES modules (id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS members;

CREATE TABLE members (
	queue VARCHAR(255),
	system VARCHAR(255),
	uuid VARCHAR(255) NOT NULL DEFAULT '',
	session_uuid VARCHAR(255) NOT NULL DEFAULT '',
	cid_number VARCHAR(255),
	cid_name VARCHAR(255),
	system_epoch INTEGER NOT NULL DEFAULT 0,
	joined_epoch INTEGER NOT NULL DEFAULT 0,
	rejoined_epoch INTEGER NOT NULL DEFAULT 0,
	bridge_epoch INTEGER NOT NULL DEFAULT 0,
	abandoned_epoch INTEGER NOT NULL DEFAULT 0,
	base_score INTEGER NOT NULL DEFAULT 0,
	skill_score INTEGER NOT NULL DEFAULT 0,
	serving_agent VARCHAR(255),
	serving_system VARCHAR(255),
	state VARCHAR(255) 
);

DROP TABLE IF EXISTS agents;

CREATE TABLE agents (
	name VARCHAR(255),
	system VARCHAR(255),
	uuid VARCHAR(255),
	type VARCHAR(255),
	contact VARCHAR(255),
	status VARCHAR(255),
	state VARCHAR(255),
	max_no_answer INTEGER NOT NULL DEFAULT 0,
	wrap_up_time INTEGER NOT NULL DEFAULT 0,
	reject_delay_time INTEGER NOT NULL DEFAULT 0,
	busy_delay_time INTEGER NOT NULL DEFAULT 0,
	no_answer_delay_time INTEGER NOT NULL DEFAULT 0,
	last_bridge_start INTEGER NOT NULL DEFAULT 0,
	last_bridge_end INTEGER NOT NULL DEFAULT 0,
	last_offered_call INTEGER NOT NULL DEFAULT 0,
	last_status_change INTEGER NOT NULL DEFAULT 0,
	no_answer_count INTEGER NOT NULL DEFAULT 0,
	calls_answered INTEGER NOT NULL DEFAULT 0,
	talk_time INTEGER NOT NULL DEFAULT 0,
	ready_time INTEGER NOT NULL DEFAULT 0,
	external_calls_count INTEGER NOT NULL DEFAULT 0
);

DROP TABLE IF EXISTS tiers;

CREATE TABLE tiers (
	queue VARCHAR(255),
	agent VARCHAR(255),
	state VARCHAR(255),
	level INTEGER NOT NULL DEFAULT 1,
	position INTEGER NOT NULL DEFAULT 1 
 );
DROP TYPE IF EXISTS type_bool;
CREATE TYPE type_bool AS ENUM ('true', 'false');
DROP TYPE IF EXISTS type_score;
CREATE TYPE type_score AS ENUM ('queue', 'system');
DROP TYPE IF EXISTS type_strategy;
CREATE TYPE type_strategy AS ENUM ('ring-all', 'longest-idle-agent', 'round-robin', 'top-down', 'agent-with-least-talk-time', 'agent-with-fewest-calls', 'sequentially-by-agent-order', 'random', 'ring-progressively');

DROP TABLE IF EXISTS queue;

CREATE TABLE queue (	
	id SERIAL NOT NULL,
	name VARCHAR(120),
	strategy type_strategy,
	moh_sound VARCHAR(120) DEFAULT 'local_stream://moh',
	record_template VARCHAR(255) DEFAULT NULL,
	time_base_score type_score,
	tier_rules_apply type_bool,
	tier_rule_wait_second INTEGER,
	tier_rule_wait_multiply_level type_bool,
	tier_rule_no_agent_no_wait type_bool,
	discard_abandoned_after INTEGER,
	abandoned_resume_allowed type_bool,
	max_wait_time INTEGER DEFAULT 0,
	max_wait_time_with_no_agent INTEGER DEFAULT 0,
	max_wait_time_with_no_agent_time_reached INTEGER DEFAULT 5,
	calls_answered VARCHAR(255),
	calls_abandoned VARCHAR(255),
	ring_progressively_delay INTEGER DEFAULT 10,
        fk_id_host VARCHAR(255),
	PRIMARY KEY (id)
);

CREATE INDEX queue_id ON queue (id);

DROP TYPE IF EXISTS type_agent;
CREATE TYPE type_agent AS ENUM ('callback', 'uuid-standby');

DROP TABLE IF EXISTS agent;

CREATE TABLE agent(
	id SERIAL NOT NULL,
	name VARCHAR(255),
	type type_agent DEFAULT 'callback', 
	contact VARCHAR(255),
	status VARCHAR(150) DEFAULT 'Available', 
	max_no_answer INTEGER, 
	wrap_up_time INTEGER, 
	reject_delay_time INTEGER, 
	busy_delay_time INTEGER,
        fk_id_host VARCHAR(255),
	PRIMARY KEY (id)
);

CREATE INDEX agent_id ON agent (id);

DROP TABLE IF EXISTS queue_agent;

CREATE TABLE queue_agent (
	id SERIAL NOT NULL,
	agent VARCHAR(120),
	queue VARCHAR(180),
	level INTEGER DEFAULT 1, 
	position INTEGER DEFAULT 1,
        fk_id_host VARCHAR(255),
	PRIMARY KEY (id)
);

CREATE INDEX queue_agent_id ON queue_agent (id);


DROP VIEW IF EXISTS vw_modules_callcenter;

CREATE VIEW vw_modules_callcenter AS (
	SELECT
		a1.fs,
		a1.type,
		a1.name,
		a1.description,
                a1.fs_key as fs_uuid,
		a2.name as module,
		a3.db,
		a3.dsn,
		a3.driver

	FROM 
		modules as a1
	LEFT JOIN
		modules_callcenter a2 ON a1.id = a2.fk_id_host
	LEFT JOIN
		modules_callcenter_setting a3 ON a1.id = a3.fk_id_host
);

DROP TABLE IF EXISTS cdr;

CREATE TABLE cdr (
	local_ip_v4 inet NOT NULL,
	caller_id_name character varying,
	caller_id_number character varying,
	destination_number character varying,
	context character varying NOT NULL,
	start_stamp timestamp with time zone NOT NULL,
	answer_stamp timestamp with time zone,
	end_stamp timestamp with time zone NOT NULL,
	duration integer NOT NULL,
	billsec integer NOT NULL,
	hangup_cause character varying NOT NULL,
	uuid uuid NOT NULL,
	bleg_uuid uuid,
	accountcode character varying,
	read_codec character varying,
	write_codec character varying,
	sip_hangup_disposition character varying,
	ani character varying
);

CREATE INDEX uuid_index ON cdr (uuid);

DROP TABLE IF EXISTS modules_format_cdr;

CREATE TABLE modules_format_cdr (
	id SERIAL NOT NULL,
	name VARCHAR(120),
	fk_id_host integer,
        PRIMARY KEY (id)
);

DROP TABLE IF EXISTS format_cdr_profile;

CREATE TABLE format_cdr_profile (
	id SERIAL NOT NULL,
	name VARCHAR(120),
        fk_id_host VARCHAR(255),
	PRIMARY KEY (id)
);

DROP TABLE IF EXISTS format_cdr_settings;

CREATE TABLE format_cdr_settings (
	id SERIAL NOT NULL,
	param_name VARCHAR(255),
	param_value VARCHAR(255),
        fk_id_host VARCHAR(255),
        fk_cdr_profile INT NOT NULL REFERENCES format_cdr_profile(id) ON DELETE CASCADE,
	PRIMARY KEY (id)
);

DROP VIEW IF EXISTS vw_modules_format_cdr;

CREATE VIEW vw_modules_format_cdr AS (
        SELECT
                a1.id,
		a1.fs,
		a1.type,
		a1.name,
		a1.description,
                a1.fs_key as fs_uuid,
		a2.name as module

	FROM 
		modules as a1
	LEFT JOIN
		modules_format_cdr a2 ON a1.id = a2.fk_id_host
        WHERE a1.name = 'format_cdr.conf'
);


DROP TABLE IF EXISTS ivr_menu;

CREATE TABLE ivr_menu (
	id SERIAL NOT NULL,
	name VARCHAR(120),
        fk_id_host VARCHAR(255),
	PRIMARY KEY (id)
);

DROP TABLE IF EXISTS ivr_menu_config;

CREATE TABLE ivr_menu_config (
	id SERIAL NOT NULL,
	param_name VARCHAR(255),
	param_value VARCHAR(255),
        fk_id_host VARCHAR(255),
        fk_menu INT NOT NULL REFERENCES ivr_menu(id) ON DELETE CASCADE,
	PRIMARY KEY (id)
);


DROP TABLE IF EXISTS ivr_menu_action;

CREATE TABLE ivr_menu_action (
	id SERIAL NOT NULL,
	action VARCHAR(255),
	digits INT NOT NULL,
        param VARCHAR(255),
        seq INT NOT NULL,
        fk_menu INT NOT NULL REFERENCES ivr_menu(id) ON DELETE CASCADE,
	PRIMARY KEY (id)
);

DROP VIEW IF EXISTS vw_ivr_menu;

CREATE VIEW vw_ivr_menu AS (
	SELECT
		a1.id as fk_id,
		a2.param_name,
		a2.param_value
	FROM 
		ivr_menu as a1
	LEFT JOIN
		ivr_menu_config a2 ON a1.id = a2.fk_menu
);

DROP TABLE IF EXISTS InfoRTP;

CREATE TABLE InfoRTP (
		host VARCHAR(120) NOT NULL,
		uuid UUID NOT NULL,
		type VARCHAR(64) NOT NULL,
		raw_bytes INTEGER,
		media_bytes INTEGER,
		packet_count INTEGER,
		media_packet_count INTEGER,
		skip_packet_count INTEGER,
		jitter_packet_count INTEGER,
		dtmf_packet_count INTEGER,
		cng_packet_count INTEGER,
		flush_packet_count INTEGER,
		largest_jb_size INTEGER,
		jitter_min_variance DOUBLE PRECISION,
		jitter_max_variance DOUBLE PRECISION,
		jitter_loss_rate DOUBLE PRECISION,
		jitter_burst_rate DOUBLE PRECISION,
		mean_interval DOUBLE PRECISION,
		flaw_total INTEGER,
		quality_percentage DOUBLE PRECISION,
		mos DOUBLE PRECISION,
		rtcp_packet_count INTEGER,
		rtcp_octet_count INTEGER
);

DROP TABLE IF EXISTS App_Log;

CREATE TABLE App_Log (
		host VARCHAR(120) NOT NULL,
		uuid UUID NOT NULL,
		app_name VARCHAR(255) DEFAULT NULL,
		app_data VARCHAR(255) DEFAULT NULL,
		app_stamp VARCHAR(255) DEFAULT NULL,
		seq_event INTEGER
);

DROP TABLE IF EXISTS Call_Info;

CREATE TABLE Call_Info (
                uuid UUID NOT NULL,
                dt_call CHARACTER VARYING,
                dt_start CHARACTER VARYING,
                dt_answer CHARACTER VARYING,
                dt_end CHARACTER VARYING, 
		ani VARCHAR(255) DEFAULT NULL,
		rdnis VARCHAR(255) DEFAULT NULL,
		duration INTEGER,
		billsec INTEGER,
                last_app CHARACTER VARYING,
                last_arg CHARACTER VARYING,
		hangup_cause CHARACTER VARYING,
		sip_term_cause INTEGER
);

CREATE INDEX uuid_info ON Call_Info (uuid);


DROP TABLE IF EXISTS queue_log;

CREATE TABLE queue_log (
		b_lang_uuid UUID NOT NULL, 
		direction CHARACTER VARYING, 
		cc_queue CHARACTER VARYING, 
		cc_side CHARACTER VARYING, 
		cc_member_uuid UUID, 
		cc_queue_joined_epoch INTEGER, 
		cc_agent_bridged CHARACTER VARYING, 
		cc_agent_uuid CHARACTER VARYING, 
		cc_agent CHARACTER VARYING, 
		cc_queue_answered_epoch INTEGER, 
		cc_queue_canceled_epoch INTEGER, 
		cc_cause CHARACTER VARYING, 
		cc_cancel_reason CHARACTER VARYING
);

CREATE INDEX cc_member_uuid_index ON queue_log (cc_member_uuid);
