<?php

/**
 *
 *      PHP is an Opensource App Freeswitch
 *
 * 	@author Samuel da Silva Mendes
 *
 * 	Copyright (c) 2018 "Module load XML Freeswitch"
 *
 * 	This file is part of Module load XML Freeswitch.
 *
 * 	Discador App Freeswitch is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * 	GNU General Public License for more details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program. If not, see <http://www.gnu.org/licenses/>
 */

namespace App\Lib\FS;

/**
 * Description of Modules
 *
 * @author p585700
 */
class Ivr extends XmlCurl {

    private $fs = \NULL;

    /**
     * Objeto de acesso ao banco
     * @db object
     * @access private
     */
    private $db = \NULL;

    public function __construct() {
        parent::__construct();
        $db = new \App\Db\Modal();
        $this->db = $db;
    }

    public function getIvrMenu($data) {
        $menu = $this->db->SQL("SELECT * FROM ivr_menu WHERE fk_id_host like '%$data->id%';");
        if (!empty($menu)) {
            $element = $this->getRoot();
            $section = $this->NewSection($data->type, "Modulo IVR");
            $config = $this->NewConfig($section, $data->name, $data->description);
            $menus = $config->addChild('menus', ' ');

            for ($i = 0; $i < count($menu); $i++) {
                $menu_conf = $this->db->vw_ivr_menu($menu[$i]->id);
                $menu_conf_element = $menus->addChild('menu', ' ');
                $menu_conf_element->addAttribute("name", $menu[$i]->name);

                $ivr_action = $this->db->ivrAction($menu[$i]->id);

                foreach ($ivr_action as $key => $value) {
                    $menu_action = $menu_conf_element->addChild('entry');
                    $menu_action->addAttribute("action", $value->action);
                    $menu_action->addAttribute("digits", $value->digits);
                    $menu_action->addAttribute("param", $value->param);
                }
                for ($b = 0; $b < count($menu_conf); $b++) {
                    $menu_conf_element->addAttribute($menu_conf[$b]->param_name, $menu_conf[$b]->param_value);
                }
            }

            print_r($this->getXML());
        } else {
            $this->setNULL();
            print_r($this->getXML());
        }
    }

}
