<?php

/**
 *
 *  Discador php is an Opensource App Mencached
 *
 * 	@author Samuel da Silva Mendes
 *
 * 	Copyright (c) 2018 "App Mencached"
 *
 * 	This file is part of App Mencached.
 *
 * 	URA App Freeswitch is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * 	GNU General Public License for more details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program. If not, see <http://www.gnu.org/licenses/>
 */

namespace App\Lib\FS;

class Mem {

    /**
     * Construct Memcached.
     *
     * @param string $host        	
     * @param string $port        	
     */
    public function __construct($host, $port) {

        /* Set Object $memcached */
        $this->memcached = new \Memcached ();
        // $this->memcached->setOption ( \Memcached::OPT_BINARY_PROTOCOL, true );

        /* Set Object $host */
        $this->srv_host = $host;

        /* Set Object $port */
        $this->srv_port = $port;

        $this->memcached->addServer($this->srv_host, $this->srv_port);
        $mc = $this->memcached->getStats();
        $open = $mc ["$this->srv_host:$this->srv_port"];

        if ($open ['pid'] == - 1) {
            echo "Connection to $host failed" . "\n";
            exit();
        }
        if ($open ['limit_maxbytes'] / 1024 / 1024 < 128) {
            echo "low memory" . "\n";
            exit();
        }
    }

    public function setObj($key, $value, $expiration) {
        if (isset($expiration) == NULL || isset($expiration) == '') {
            $expiration = 1200;
        }
        if (isset($key) && isset($value)) {
            $value = $this->memcached->add($key, $value, $expiration);
        } else {
            echo "No object insert Mem" . "\n";
        }
    }

    public function getObj($obj) {
        return $this->memcached->get($obj);
    }

    public function delObj($obj, $time) {
        if (isset($time) == NULL || isset($time) == '') {
            $time = 0;
        }
        $this->memcached->delete($obj, $time);
    }

    public function setArrayObj($mz, $values, $expiration) {
        if (isset($expiration) == NULL || isset($expiration) == '') {
            $expiration = 1200;
        }
        if (isset($mz) && isset($values)) {
            $value = $this->memcached->set($mz, $values, $expiration);
        } else {
            echo "No array insert Mem" . "\n";
        }
    }

}
