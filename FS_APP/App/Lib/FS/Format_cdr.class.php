<?php

/**
 *
 *     PHP is an Opensource App Freeswitch
 *
 * 	@author Samuel da Silva Mendes
 *
 * 	Copyright (c) 2018 "Module Format_cdr XML Freeswitch"
 *
 * 	This file is part of Module Format_cdr XML Freeswitch.
 *
 * 	 Discador App Freeswitch is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * 	GNU General Public License for more details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program. If not, see <http://www.gnu.org/licenses/>
 */

namespace App\Lib\FS;

/**
 * Classe responsavel pela configuraçoes do mod_format_cdr.
 */
class Format_cdr extends XmlCurl {

    /**
     * Variavel contrurora da classe Conexao();
     * @var type Object,
     */
    private $db = \NULL;

    /**
     * VAriavel contrutora da tag root xml.
     * @var type object.
     */
    private $element = \NULL;

    public function __construct() {
        /**
         * Função contrutora da classe
         * Estende o contrutor da classe Conexao().
         * Faz o SET da variavel $this->db.
         */
        parent::__construct();
        $db = new \App\Db\Modal();
        $this->db = $db;
    }

    /**
     * Função para criar a tags Profiles
     * @param type $tagSection objeto $this->config.
     * @return type object.
     */
    public function setProfiles($tagSection) {
        $profiles = $tagSection->addChild('profiles', ' ');
        $this->profiles = $profiles;
        return $this->profiles;
    }

    /**
     * Função para criar a tag Profile
     * @param type $tagSection objeto $this->profiles
     * @param type $data nome do atributo
     * @return type object
     */
    public function addProfile($tagSection, $data) {
        $profile = $tagSection->addChild('profiles', ' ');
        $profile->addAttribute('name', $data);
        $this->profile = $profile;
        return $this->profile;
    }

    /**
     * Função para criar a tag configuration
     * @param type $data receber o object da visão vw_modules_format_cdr.
     */
    public function format($data) {
        $this->element = $this->getRoot();
        $section = $this->NewSection($data->type, "Modulo Format CDR");
        $config = $this->NewConfig($section, $data->name, $data->description);
        $profiles = $this->setProfiles($config);

        /**
         * Criação dos profiles apartir das configuraçoes do host
         */
        $profile = $this->db->getHost("format_cdr_profile", $data->id);
        $params = $this->db->SQL("SELECT * FROM format_cdr_settings WHERE fk_id_host LIKE '%$data->id%';");
        if (!empty($profile)) {
            for ($a = 0; $a < count($profile); $a++) {
                $this->setProfile($profiles, $profile[$a], $params);
            }
        }
        print_r($this->getXML());
    }

    /**
     * Funcação para contruir as tags profile.
     * @param type $element objeto $this->profiles.
     * @param type $data objeto $this->profile.
     * @param type $params objeto $this->params.
     * @return type Object
     */
    public function setProfile($element, $data, $params) {
        $profile_s = $element->addChild('profile');
        $profile_s->addAttribute('name', $data->name);
        $prof_set = $profile_s->addChild('settings', ' ');

        for ($a = 0; $a < count($params); $a++) {
            if ($data->id == $params[$a]->fk_cdr_profile) {
                $this->addParam($prof_set, $params[$a]->param_name, $params[$a]->param_value);
            }
        }
        $this->profile_s = $profile_s;
        return $this->profile_s;
    }

}
