<?php
/**
 *
 *  Discador php is an Opensource App Freeswitch
 *
 *	@author Samuel da Silva Mendes
 *
 *	Copyright (c) 2018 "Socket App Freeswitch"
 *
 *	This file is part of Socket App Freeswitch.
 *
 *	Discador App Freeswitch is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program. If not, see <http://www.gnu.org/licenses/>
 */

namespace App\Lib\FS;

class Socket {
	public function __construct() {
		$this->port = NULL;
		$this->host = NULL;
		$this->pwd = NULL;
	}
	private function setAddress($address) {
		$this->host = $address;
	}
	private function setPort($port) {
		$this->port = $port;
	}
	private function setPassword($pwd) {
		$this->pwd = $pwd;
	}
	private function socket_create($port, $host, $password) {
		$this->socket = fsockopen ( $host, $port, $errno, $errdesc ) or die ( "CONNECTION TO $host FAILED" );
		socket_set_blocking ( $this->socket, false );
		
		if ($this->socket) {
			while ( ! feof ( $this->socket ) ) {
				$buffer = fgets ( $this->socket, 1024 );
				usleep ( 100 ); // allow time for reponse
				if (trim ( $buffer ) == "Content-Type: auth/request") {
					fputs ( $this->socket, "auth $password\n\n" );
					break;
				}
			}
			return $this->socket;
		} else {
			return false;
		}
	}
	private function socket_request($socket, $cmd) {
		if ($socket) {
			fputs ( $socket, "api " . $cmd . "\n\n" );
			usleep ( 100 );
			
			$this->response = "";
			$i = 0;
			$this->contentlength = 0;
			while ( ! feof ( $socket ) ) {
				$this->buffer = fgets ( $socket, 4096 );
				if ($this->contentlength > 0) {
					$this->response .= $this->buffer;
				}
				
				if ($this->contentlength == 0) {
					if (strlen ( trim ( $this->buffer ) ) > 0) {
						$this->temparray = explode ( ":", trim ( $this->buffer ) );
						if ($this->temparray [0] == "Content-Length") {
							$this->contentlength = trim ( $this->temparray [1] );
						}
					}
				}
				
				usleep ( 100 );
				
				if ($i > 10000) {
					break;
				}
				
				if ($this->contentlength > 0) {
					
					if (strlen ( $this->response ) >= $this->contentlength) {
						break;
					}
				}
				$i ++;
			}
			return $this->response . "\n\n";
		} else {
			echo "NO HANDLE";
		}
	}
	public function connect($host, $port, $pwd) {
		if (empty ( $host ) || empty ( $port ) || empty ( $pwd )) {
			die ( "ERROR CONFIG FILE!" );
		}
		self::setAddress ( $host );
		self::setPort ( $port );
		self::setPassword ( $pwd );
	}
	public function Command($cmd) {
		$this->fp = self::socket_create ( $this->port, $this->host, $this->pwd );
		echo self::socket_request ( $this->fp, $cmd );
	}
	public function close() {
		fclose ( $this->socket );
	}
	public function setReloadXML() {
		$this->Command ( "reloadxml" );
	}
	public function setReloadCallcenter() {
		$this->Command ( "reload mod_callcenter" );
	}
}
