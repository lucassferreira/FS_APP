<?php

/**
 *
 *  Discador php is an Opensource App Freeswitch
 *
 * 	@author Samuel da Silva Mendes
 *
 * 	Copyright (c) 2017 "Discador App Freeswitch"
 *
 * 	This file is part of Discador App Freeswitch.
 *
 * 	 Discador App Freeswitch is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * 	GNU General Public License for more details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program. If not, see <http://www.gnu.org/licenses/>
 */

namespace App\Lib\FS;

class DialFS extends Socket {

    private $conf;
    private $route;
    private $callback;
    private $campanha;

    public function __construct($host, $port, $pwd) {
        $this->connect($host, $port, $pwd);
    }

    public function setArraySrc($data = array()) {
        if (is_array($data)) {
            $conf = '';
            if (count($data) == 1) {
                foreach ($data as $key => $param) {
                    $conf .= $key . '=' . $param;
                }
            } else {
                foreach ($data as $key => $param) {
                    $conf .= $key . rtrim("=") . $param . rtrim(",");
                }
            }
            $conf_param = "{" . substr($conf, 0, - 1) . "}";
            $this->conf = $conf_param;
        } else {
            return $this->conf = NULL;
        }
    }

    public function setRota($trk) {
        if (!empty($trk)) {
            $this->route = $trk;
        }
    }

    public function setCallback($callback) {
        if (!empty($callback)) {
            $this->callback = $callback;
        }
    }

    public function setCampanha($data) {
        if (!empty($data)) {
            return $this->campanha = $data;
        } else {
            return NULL;
        }
    }

    public function Dial($number, $context) {
        if (!empty($number) != NULL && !empty($context) != NULL) {
            $cmd = "originate {sip_h_X-FS-Campanha=" . $this->campanha . "}" . $this->conf . "sofia/gateway/$this->route/$number 001 XML $context CALLER_ID_NAME CALLER_ID_NUMBER";
            $this->Command($cmd);
        } else {
            exit();
        }
    }

}
