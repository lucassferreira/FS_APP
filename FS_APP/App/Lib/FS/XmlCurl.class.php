<?php

/**
 *
 *  Discador php is an Opensource App Freeswitch
 *
 * 	@author Samuel da Silva Mendes
 *
 * 	Copyright (c) 2017 "XML App Freeswitch"
 *
 * 	This file is part of XML App Freeswitch.
 *
 * 	Discador App Freeswitch is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * 	GNU General Public License for more details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program. If not, see <http://www.gnu.org/licenses/>
 */

namespace App\Lib\FS;

class XmlCurl {

    /**
     * Tag Root XML doc.
     *
     * @var object
     */
    private $tagRoot;

    /**
     * Tag Section Root XML doc.
     *
     * @var object
     */
    private $section;

    /**
     * Tag Context Root XML doc.
     *
     * @var object
     */
    private $context;

    /**
     * Tag Configuratio Root XML doc.
     *
     * @var object
     */
    private $configuration;

    /**
     * New tag generic
     *
     * @var object
     */
    private $newTag;

    /**
     * New element Param
     * @var object 
     */
    private $newParam;

    /**
     * New tah settings;
     * @var object 
     */
    private $settings;

    /**
     * Construtor do header + tag Root do XML.
     */
    public function __construct() {

        // Tag root do XML freeswitch
        header('Content-Type: text/xml');
        $tagRoot = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" standalone="no"?><document />');
        $tagRoot->addAttribute('type', 'freeswitch/xml');
        $this->tagRoot = $tagRoot;
    }

    public function setNULL() {
        /**
         * Resposta Not Found
         *
         * @var Configs $XML
         */
        $section = $this->tagRoot->addChild('section', ' ');
        $section->addAttribute('name', 'result');
        $result = $section->addChild('result ');
        $result->addAttribute('status', 'not found');
        return $result;
    }

    /**
     * Retorna a tag Root
     *
     * @return object|SimpleXMLElement
     */
    public function getRoot() {
        return $this->tagRoot;
    }

    /**
     * New tag context XML
     *
     * @param string $name        	
     * @param string $desc        	
     */
    public function NewSection($name, $desc = NULL) {
        $section = $this->tagRoot->addChild('section', ' ');
        if (isset($desc) != NULL) {
            $newDesc = utf8_encode($desc);
            $section->addAttribute('name', $name);
            $section->addAttribute('description', $newDesc);
        } else {
            $section->addAttribute('name', $name);
        }
        $this->section = $section;
        return $this->section;
    }

    /**
     * Return Tag Section
     *
     * @return object|SimpleXMLElement
     */
    public function getSection() {
        return $this->section;
    }

    /**
     * Print tag Root XML
     *
     * @param object $root        	
     * @return object|boolean
     */
    public function RootXml($root) {
        if (!empty($root)) {
            return $root->asXML();
        } else {
            return false;
        }
    }

    /**
     * Set tag new context
     *
     * @param object $tagSection        	
     * @param string $name        	
     * @return object
     */
    public function NewContext($tagSection, $name) {
        if (!empty($tagSection)) {
            $context = $tagSection->addChild('context', ' ');
            $context->addAttribute('name', $name);
        } else {
            exit();
        }
        $this->context = $context;
        return $this->context;
    }

    /**
     * Set tag new configuration
     *
     * @param object $tagSection        	
     * @param string $name        	
     * @param string $desc        	
     * @return object
     */
    public function NewConfig($tagSection, $name, $desc = NULL) {
        if (!empty($tagSection)) {
            $configuration = $tagSection->addChild('configuration', ' ');
            $configuration->addAttribute('name', $name);
            if ($desc != NULL) {
                $newDesc = utf8_encode($desc);
                $configuration->addAttribute('description', $newDesc);
            }
        } else {
            exit();
        }
        $this->configuration = $configuration;
        return $this->configuration;
    }

    /**
     * Add new Tag generic
     *
     * @param object $tagSection        	
     * @param string $nametag        	
     * @param string $text
     *        	| boolean
     * @param array $dados        	
     * @return object
     */
    public function addNewTag($tagSection, $nametag, $text = NULL, $dados = array()) {
        if (!empty($tagSection)) {
            $newText = utf8_encode($text);
            $new_nametag = utf8_encode($nametag);
            if (isset($text)) {
                $newTag = $tagSection->addChild($new_nametag, $newText);
            } elseif ($text == false) {
                $newTag = $tagSection->addChild($new_nametag);
            } else {
                $newTag = $tagSection->addChild($new_nametag, ' ');
            }
            if (is_array($dados)) {
                foreach ($dados as $key => $is_obj) {
                    $newTag->addAttribute($key, $is_obj);
                }
            }
        } else {
            exit();
        }
        $this->newTag = $newTag;
        return $this->newTag;
    }

    public function addSettings($tagSection) {
        if (!empty($tagSection)) {
            $settings = $tagSection->addChild('settings', ' ');
        } else {
            exit();
        }
        $this->settings = $settings;
        return $this->settings;
    }

    public function addParam($tagSection, $name, $value) {
        $new_name = utf8_encode($name);
        $new_value = utf8_encode($value);
        if (!empty($tagSection)) {
            $param = $tagSection->addChild('param');
            $param->addAttribute('name', $new_name);
            $param->addAttribute('value', $new_value);
        } else {
            exit();
        }
        $this->newParam = $param;
        return $this->newParam;
    }

    public function addQueues($tagSection) {
        $Queues = $tagSection->addChild('queues', ' ');
        $this->Queues = $Queues;
        return $this->Queues;
    }

    public function addTiers($tagSection) {
        $Tiers = $tagSection->addChild('tiers', ' ');
        $this->Tiers = $Tiers;
        return $this->Tiers;
    }

    public function addAgents($tagSection) {
        $Agents = $tagSection->addChild('agents', ' ');
        $this->Agents = $Agents;
        return $this->Agents;
    }

    public function getXML() {
        print_r($this->tagRoot->asXML());
    }

    /**
     * Destruct tag Root
     */
    public function __destruct() {
        $this->tagRoot = NULL;
    }

}
