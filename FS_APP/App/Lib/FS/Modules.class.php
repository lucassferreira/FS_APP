<?php

/**
 *
 *  Discador php is an Opensource App Freeswitch
 *
 * 	@author Samuel da Silva Mendes
 *
 * 	Copyright (c) 2018 "Module load XML Freeswitch"
 *
 * 	This file is part of Module load XML Freeswitch.
 *
 * 	 Discador App Freeswitch is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * 	GNU General Public License for more details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program. If not, see <http://www.gnu.org/licenses/>
 */

namespace App\Lib\FS;

/**
 * Description of Modules
 *
 * @author p585700
 */
class Modules extends XmlCurl {

    /**
     * Receber a variavel $_POST
     * @var string
     * @access private
     */
    private $post = \NULL;

    /**
     * Hostname Request
     * @var string
     * @access private
     */
    private $hostname = '';

    /**
     * Type Request
     * @var string
     * @access private
     */
    private $section = '';

    /**
     * Tag name Reques
     * @var string
     * @access private
     */
    private $tag_name = '';

    /**
     * Key name Request
     * @var string
     * @access private
     */
    private $key_name = '';

    /**
     * Key value Request set object
     * @var string
     * @access private
     */
    private $key_value = '';

    /**
     * Objeto de acesso ao banco
     * @db object
     * @access private
     */
    private $db = \NULL;

    public function __construct($post) {
        parent::__construct();
        if (array_filter($post)) {
            $this->hostname = $post["hostname"];
            $this->section = $post["section"];
            $this->key_name = $post["key_name"];
            $this->key_value = $post["key_value"];
            $this->tag_name = $post["tag_name"];
        } else {
            $this->setNULL();
            print_r($this->getXML());
        }
        $db = new \App\Db\Modal();
        $this->db = $db;
    }

    public function getModules() {
        return $this->key_value;
    }

    public function setModules($data) {
        if (!empty($data)) {
            for ($i = 0; $i < count($data); $i++) {
                if ($this->hostname == $data[$i]->fs) {
                    if ($data[$i]->type == "configuration") {
                        switch ($data[$i]->name) {
                            case "callcenter.conf":
                                $this->callcenter($data[$i]);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        } else {
            $this->setNULL();
            print_r($this->getXML());
        }
    }

    public function callcenter($data) {

        /**
         * localizar na view as confs
         * @var object $callcenter
         */
        $callcenter = $this->db->vw_callcenter($data->fs, 'callcenter');

        /** XML configuracao basica */
        $element = $this->getRoot();
        $section = $this->NewSection($callcenter->type, "Modulo callcenter");
        $config = $this->NewConfig($section, $callcenter->name, $callcenter->description);
        $settings = $this->addSettings($config);
        $queues = $this->addQueues($config);
        $agents = $this->addAgents($config);
        $tiers = $this->addTiers($config);

        $filas = $this->db->getFilas($data->id);
        $agent = $this->db->getAgents($data->id);
        $queue_agent = $this->db->getQueueAgent($data->id);

        if (!empty($filas)) {
            for ($a = 0; $a < count($filas); $a++) {
                $this->setFilas($queues, $filas[$a]);
            }
        }

        if (!empty($agent)) {
            for ($b = 0; $b < count($agent); $b++) {
                $this->setAgents($agents, $agent[$b]);
            }
        }

        if (!empty($queue_agent)) {
            for ($c = 0; $c < count($queue_agent); $c++) {
                $this->setTiers($tiers, $queue_agent[$c]);
            }
        }

        $this->addParam($settings, $callcenter->dsn, $callcenter->driver);
        $this->addParam($settings, "cc-instance-id", $callcenter->fs_uuid = !empty($callcenter->fs_uuid) ? $callcenter->fs_uuid : mt_rand());
        print_r($this->getXML());
    }

    public function setFilas($element, $data) {
        $fila = $element->addChild('queue');
        $fila->addAttribute('name', $data->name);
        $this->addParam($fila, "strategy", $data->strategy);
        $this->addParam($fila, "moh-sound", $data->moh_sound);
        $this->addParam($fila, "record-template", $data->record_template);
        $this->addParam($fila, "time-base-score", $data->time_base_score);
        $this->addParam($fila, "tier-rules-apply", $data->tier_rules_apply);
        $this->addParam($fila, "tier-rule-wait-second", $data->tier_rule_wait_second);
        $this->addParam($fila, "tier-rule-wait-multiply-level", $data->tier_rule_wait_multiply_level);
        $this->addParam($fila, "tier-rule-no-agent-no-wait", $data->tier_rule_no_agent_no_wait);
        $this->addParam($fila, "discard-abandoned-after", $data->discard_abandoned_after);
        $this->addParam($fila, "abandoned-resume-allowed", $data->abandoned_resume_allowed);
        $this->addParam($fila, "max-wait-time", $data->max_wait_time);
        $this->addParam($fila, "max-wait-time-with-no-agent", $data->max_wait_time_with_no_agent);
        $this->addParam($fila, "max-wait-time-with-no-agent-time-reached", $data->max_wait_time_with_no_agent_time_reached);
        $this->addParam($fila, "calls-answered", $data->calls_answered_character);
        $this->addParam($fila, "calls-abandoned", $data->calls_abandoned);
        $this->addParam($fila, "ring-progressively-delay", $data->ring_progressively_delay);
        $this->fila = $fila;
        return $this->fila;
    }

    public function setAgents($element, $data) {
        $agent = $element->addChild('agent');
        $agent->addAttribute("name", $data->name);
        $agent->addAttribute("type", $data->type);
        $agent->addAttribute("contact", $data->contact);
        $agent->addAttribute("status", $data->status);
        $agent->addAttribute("max-no-answer", $data->max_no_answer);
        $agent->addAttribute("wrap-up-time", $data->wrap_up_time);
        $agent->addAttribute("reject-delay-time", $data->reject_delay_time);
        $agent->addAttribute("busy-delay-time", $data->busy_delay_time);
        $this->agent = $agent;
        return $this->agent;
    }

    public function setTiers($element, $data) {
        $tier = $element->addChild('tier');
        $tier->addAttribute("agent", $data->agent);
        $tier->addAttribute("queue", $data->queue);
        $tier->addAttribute("level", $data->level);
        $tier->addAttribute("position", $data->position);
        $this->tier = $tier;
        return $this->tier;
    }

}
