<?php

/**
 *
 *      PHP is an Opensource App Conexao DB PGSQL
 *
 * 	@author Samuel da Silva Mendes
 *
 * 	Copyright (c) 2018 "App Conexao DB PGSQL"
 *
 * 	This file is part of App Conexao DB PGSQL.
 *
 * 	URA App Asterisk is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * 	GNU General Public License for more details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program. If not, see <http://www.gnu.org/licenses/>
 */

namespace App\Db;

/**
 * Classe responsavel pela concexão com o Banco de Dados.
 * Compatibilidade com o PosgresSQL.
 * 
 */
class Conexao {

    /**
     * FQDN ou IP do Host.
     * @var type String. 
     */
    private static $HOST = NULL;

    /**
     * Nome do Base de Dados.
     * @var type String. 
     */
    private static $BD = NULL;

    /**
     * Usuario do Banco.
     * @var type String.
     */
    private static $USER = NULL;

    /**
     * Senha da Base de Dados.
     * @var type String.
     */
    private static $PASS = NULL;

    /**
     * DRIVER de conexão com o Banco.
     * @var type String.
     */
    private static $DNS = NULL;

    /**
     * Classe responsavel por data e horas core do PHP.
     * @var type Object.
     */
    public $date;

    /**
     * Variavel ao qual ira receber o tipo objeto do PDO do core do PHP.
     * @var type Object.
     */
    private static $Connect = NULL;

    function __construct() {
        self::$HOST = DB_HOST;
        self::$BD = DB_BD;
        self::$USER = DB_USER;
        self::$PASS = DB_PASS;
        self::$DNS = DB_DNS;
    }

    /**
     * Conecta com o banco de dados com o pattern singleton.
     * Retorna um objeto PDO!
     */
    private static function Conectar() {
        try {
            if (self::$Connect == NULL) :
                $dsn = self::$DNS . ':host=' . self::$HOST . ';dbname=' . self::$BD;
                self::$Connect = new \PDO($dsn, self::$USER, self::$PASS);

            endif;
        } catch (PDOException $e) {
            $date = new \DateTime ();
            echo "\033[01;31m" . $date->format('  H : i:s') . "\033[0m" . "\033[01;32m" . " -> Codigo => {$e->getCode()}" . "\033[0m" . "\t\n";
            echo "\033[01;31m" . $date->format('H:i:s') . "\033[0m" . "\033[01;32m" . " -> Menssagem => {$e->getMessage()}" . "\033[0m" . "\t\n";
            echo "\033[01;31m" . $date->format('H:i:s') . "\033[0m" . "\033[01;32m" . " -> Aquivo => {$e->getFile()}" . "\033[0m" . "\t\n";
            echo "\033[01;31m" . $date->format('H:i:s') . "\033[0m" . "\033[01;32m" . " -> Linha = > {$e->getLine()}" . "\033[0m" . "\t\n";
            die();
        }

        self::$Connect->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return self::$Connect;
    }

    /**
     * Retorna um objeto PDO Singleton Pattern.
     */
    public function getConn() {
        return self::Conectar();
    }

    /**
     * Função para esecutar um tipo BEGIN TRANSACTION.
     * @param $value = String
     * @return type boolean.
     */
    public function setExec($value) {
        return self::$Connect->exec($value);
    }

}
