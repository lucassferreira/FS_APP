<?php

/**
 *
 *      PHP is an Opensource App Modal PGSQL
 *
 * 	@author Samuel da Silva Mendes
 *
 * 	Copyright (c) 2018 "App Modal PGSQL"
 *
 * 	This file is part of App Modal PGSQL.
 *
 * 	URA App Asterisk is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * 	GNU General Public License for more details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program. If not, see <http://www.gnu.org/licenses/>
 */

namespace App\Db;

/**
 * Classe responsavel pela Modal do Banco.
 * 
 */
class Modal extends Conexao {

    /**
     * Variavel contrurora da classe Conexao();
     * @var type Object,
     */
    public $DB = NULL;

    public function __construct() {
        /**
         * Função contrutora da classe
         * Estende o contrutor da classe Conexao().
         * Faz o SET da variavel $this->DB.
         */
        parent::__construct();
        $this->DB = $this->getConn();
    }

    /*
     * Função para passar parametros sql direto.
     * função executa o prepare com objeto seguro.
     * @param $data = String
     */

    public function SQL($data) {
        $result = $this->DB->prepare($data);
        $result->execute();
        return $result->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * Função para consultar os modulos da tabela modules.
     * @param type $name nome do modulo.
     * @return type Object.
     */
    public function Module($name) {
        $result = $this->DB->prepare("SELECT * FROM modules WHERE name = '$name'");
        $result->execute();
        return $result->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * Função para carregar a visão do arquivo conf do modulo callcenter.
     * @param type $name nome do host.
     * @param type $modulo nome do modulo [ex: callcenter.conf ...]
     * @return type Object
     */
    public function vw_callcenter($name, $modulo) {
        $result = $this->DB->prepare("SELECT * FROM vw_modules_callcenter WHERE fs = '$name' AND module = '$modulo';");
        $result->execute();
        return $result->fetch(\PDO::FETCH_OBJ);
    }

    /**
     * Função para carregar a visão do arquivo conf do modulo format_cdr.
     * @param type $name nome do host.
     * @return type Object.
     */
    public function vw_modules_format_cdr($name) {
        $result = $this->DB->prepare("SELECT * FROM vw_modules_format_cdr WHERE fs = '$name';");
        $result->execute();
        return $result->fetch(\PDO::FETCH_OBJ);
    }

    /**
     * Função para retornar os dados da tabela queue pelo fk_id_host.
     * @param type $id numero do id do relacionamento.
     * @return type Object.
     */
    public function getFilas($id) {
        $result = $this->DB->prepare("SELECT * FROM queue WHERE fk_id_host LIKE '%$id%';");
        $result->execute();
        return $result->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * Função para retornar os dados da tabela agent pelo fk_id_host.
     * @param type $id numero do id do relacionamento.
     * @return type Object.
     */
    public function getAgents($id) {
        $result = $this->DB->prepare("SELECT * FROM agent WHERE fk_id_host LIKE '%$id%' ORDER BY id ASC;");
        $result->execute();
        return $result->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * Função para retornar os dados da tabela queue_agent pelo fk_id_host.
     * @param type $id numero do id do relacionamento.
     * @return type Object.
     */
    public function getQueueAgent($id) {
        $result = $this->DB->prepare("SELECT * FROM queue_agent WHERE fk_id_host LIKE '%$id%';");
        $result->execute();
        return $result->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * Função para retornar os dados da tabela $agent pelo fk_id_host.
     * @param type $table Nome da tabela.
     * @param type $id numero do id do relacionamento.
     * @return type Object.
     */
    public function getHost($table, $id) {
        $result = $this->DB->prepare("SELECT * FROM $table WHERE fk_id_host LIKE '%$id%';");
        $result->execute();
        return $result->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * Função retorna o noem do mudulo para custrção do arquivo ivr.conf
     * @param type $fs nome do host
     * @param type $module nome do modulo ex: ivr ...
     * @return type Object,
     */
    public function getFsIvr($fs, $module) {
        $result = $this->DB->prepare("SELECT * FROM modules WHERE fs = '$fs' AND name = '$module';");
        $result->execute();
        return $result->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * Função para carregar a visão do arquivo conf do menu do ivr.
     * @param type $id numero do id do relacionamento da tabela ivr_menu.
     * @return type Object.
     */
    public function vw_ivr_menu($id) {
        $result = $this->DB->prepare("SELECT * FROM vw_ivr_menu WHERE fk_id = '$id';");
        $result->execute();
        return $result->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * Função para carregar as açoes para contrução do arquivo conf do ivr.conf.
     * @param type $id numero do id do relacionamento da tabela ivr_menu.
     * @return type Object.
     */
    public function ivrAction($id) {
        $result = $this->DB->prepare("SELECT distinct action, digits, param, seq FROM ivr_menu_action WHERE fk_menu = '$id' order by digits, seq;");
        $result->execute();
        return $result->fetchAll(\PDO::FETCH_OBJ);
    }

}
