<?php

/**
 *
 *      PHP is an Opensource App cdr configs
 *
 * 	@author Samuel da Silva Mendes
 *
 * 	Copyright (c) 2018 "App cdr configs"
 *
 * 	This file is part App cdr configs.
 *
 * 	URA App Asterisk is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * 	GNU General Public License for more details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program. If not, see <http://www.gnu.org/licenses/>
 */
require ("config.php");

/**
 * Carregar os objetos da classe Conexao()
 */
$db = new App\Db\Conexao();

/**
 * Carregar objeto conexao da classe Conexao()
 */
$conn = $db->getConn();

/**
 * Carregar os objetos da classe Modal()
 */
$sql = new App\Db\Modal();

/**
 * Receber o Content-Type: application/x-www-form-plaintext e transformar e vetor
 * Pos tratartiva gera a trasação de inserte para base.
 */
//$raw = $HTTP_RAW_POST_DATA;
$raw = file_get_contents("php://input");
$row_array = array_filter(explode("cdr=", $raw));
$xml = $row_array[1];
$dom = new SimpleXMLElement($xml);

/**
 * Função para extração dos atributos do objecto arary.
 * @param type $object
 * @param type $attribute
 * @return type string.
 */
function xml_attribute($object, $attribute) {
    if (isset($object[$attribute])) {
        return (string) $object[$attribute];
    }
}

function getValue($obj) {
    $data = json_decode(json_encode($obj), true);
    $data = urldecode($data[0]);
    return $data;
}

$host_request = xml_attribute($dom, 'switchname');
$response_array = json_decode(json_encode($dom), true);

// gera os dados para montar tabela da midia e call_info

$b_lang_uuid = getValue($dom->variables->call_uuid);
$ani = getValue($dom->callflow->caller_profile->ani);
$rdnis = getValue($dom->callflow->caller_profile->rdnis);
// Pegar o rdnis pelo callee
if (empty($rdnis)) {
    $rdnis = getValue($dom->callflow->caller_profile->callee_id_number);
}
$duration = getValue($dom->variables->duration);
$billsec = getValue($dom->variables->billsec);
$last_app = getValue($dom->variables->last_app);
$hangup_cause = getValue($dom->variables->hangup_cause);
$sip_term_cause = getValue($dom->variables->hangup_cause_q850);
$current_application = getValue($dom->variables->current_application);
$direction = getValue($dom->variables->direction);
$status = getValue($dom->variables->sip_invite_failure_status);
$dt_call = date("d-m-Y", strtotime(getValue($dom->variables->start_stamp)));
$dt_start = date("H:i:s", strtotime(getValue($dom->variables->start_stamp)));
$dt_answer = date("H:i:s", strtotime(getValue($dom->variables->answer_stamp)));
$dt_end = date("H:i:s", strtotime(getValue($dom->variables->end_stamp)));

$rtp = $response_array["call-stats"]["audio"];

if ($rtp["inbound"]) {
    $rtp_in = $rtp["inbound"];
    $data = "INSERT INTO infortp(
            host, uuid, type, raw_bytes, media_bytes, packet_count, media_packet_count, 
            skip_packet_count, jitter_packet_count, dtmf_packet_count, cng_packet_count, 
            flush_packet_count, largest_jb_size, jitter_min_variance, jitter_max_variance, 
            jitter_loss_rate, jitter_burst_rate, mean_interval, flaw_total, 
            quality_percentage, mos, rtcp_packet_count, rtcp_octet_count)
    VALUES ('$host_request', '$b_lang_uuid' , 'inbound', ${rtp_in["raw_bytes"]}, ${rtp_in["media_bytes"]}, ${rtp_in["packet_count"]}, ${rtp_in["media_packet_count"]}, 
            ${rtp_in["skip_packet_count"]}, ${rtp_in["jitter_packet_count"]}, ${rtp_in["dtmf_packet_count"]}, ${rtp_in["cng_packet_count"]}, 
            ${rtp_in["flush_packet_count"]}, ${rtp_in["largest_jb_size"]}, ${rtp_in["jitter_min_variance"]}, ${rtp_in["jitter_max_variance"]}, 
            ${rtp_in["jitter_loss_rate"]}, ${rtp_in["jitter_burst_rate"]}, ${rtp_in["mean_interval"]}, ${rtp_in["flaw_total"]}, 
            ${rtp_in["quality_percentage"]}, ${rtp_in["mos"]}, NULL, NULL);
";
    $sql->SQL($data);
}
if ($rtp["outbound"]) {
    $rtp_out = $rtp["outbound"];
    $data = "INSERT INTO infortp(
            host, uuid, type, raw_bytes, media_bytes, packet_count, media_packet_count, 
            skip_packet_count, jitter_packet_count, dtmf_packet_count, cng_packet_count, 
            flush_packet_count, largest_jb_size, jitter_min_variance, jitter_max_variance, 
            jitter_loss_rate, jitter_burst_rate, mean_interval, flaw_total, 
            quality_percentage, mos, rtcp_packet_count, rtcp_octet_count)
    VALUES ('$host_request', '$b_lang_uuid' , 'outbound', ${rtp_out["raw_bytes"]}, ${rtp_out["media_bytes"]}, ${rtp_out["packet_count"]}, ${rtp_out["media_packet_count"]}, 
            ${rtp_out["skip_packet_count"]}, NULL, ${rtp_out["dtmf_packet_count"]}, ${rtp_out["cng_packet_count"]}, 
            NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, 
            NULL, NULL, ${rtp_out["rtcp_packet_count"]}, ${rtp_out["rtcp_octet_count"]});
";
    $sql->SQL($data);
}

// gerar os dados para montar tabela da App_Log

$app_log = $response_array["app_log"]["application"];


$contador = 1;
for ($i = 0; $i < count($app_log); $i++) {
    $app_name = xml_attribute($app_log[$i]["@attributes"], 'app_name');
    $app_data = xml_attribute($app_log[$i]["@attributes"], 'app_data');
    $app_stamp = xml_attribute($app_log[$i]["@attributes"], 'app_stamp');
    $seq_event = $contador++;
    $AppInsertLog .= "INSERT INTO app_log(host, uuid, app_name, app_data, app_stamp, seq_event)
    VALUES ('$host_request', '$b_lang_uuid', '$app_name', '$app_data', '$app_stamp', $seq_event);" . "\n";
}

$AppInsertLog = "BEGIN;" . "\n" . $AppInsertLog . "COMMIT;";
$db->setExec($AppInsertLog);

if ($duration > 0 && $billsec > 0) {
    $callinfo = "INSERT INTO call_info(
             uuid, dt_call, dt_start, dt_answer, dt_end, ani, rdnis, duration, billsec, last_app, last_arg, hangup_cause, 
            sip_term_cause)
    VALUES ('$b_lang_uuid', '$dt_call', '$dt_start', '$dt_answer', '$dt_end', '$ani', '$rdnis', $duration, $billsec, '$last_app', '$last_arg','$hangup_cause', $sip_term_cause)";

    $sql->SQL($callinfo);
} else {
    if ($status !== "404") {
        // Gravar as chamadas não atendidas excluido falha do endpoint não disponivel.
        $callinfo = "INSERT INTO call_info(
             uuid, dt_call, dt_start, dt_answer, dt_end, ani, rdnis, duration, billsec, last_app, last_arg, hangup_cause, 
            sip_term_cause)
        VALUES ('$b_lang_uuid', '$dt_call', '$dt_start', NULL, '$dt_end', '$ani', '$rdnis', $duration, $billsec, '$last_app', '$last_arg','$hangup_cause', $sip_term_cause)";

        $sql->SQL($callinfo);
    }
}

if ($direction == "inbound" && $current_application == "callcenter") {
    $cc_queue = getValue($dom->variables->cc_queue);
    $cc_side = getValue($dom->variables->cc_side);
    $cc_member_uuid = getValue($dom->variables->cc_member_uuid);
    
    $cc_queue_joined_epoch = getValue($dom->variables->cc_queue_joined_epoch);
    if (empty($cc_queue_joined_epoch)) {
        $cc_queue_joined_epoch = 0;
    }
    $cc_agent_bridged = getValue($dom->variables->cc_agent_bridged);
    $cc_agent_uuid = getValue($dom->variables->cc_agent_uuid);
    $cc_agent = getValue($dom->variables->cc_agent);
    
    $cc_queue_answered_epoch = getValue($dom->variables->cc_queue_answered_epoch);
    if (empty($cc_queue_answered_epoch)) {
        $cc_queue_answered_epoch = 0;
    }
    $cc_queue_canceled_epoch = getValue($dom->variables->cc_queue_canceled_epoch);
    if (empty($cc_queue_canceled_epoch)) {
        $cc_queue_canceled_epoch = 0;
    }
    $cc_cause = getValue($dom->variables->cc_cause);
    $cc_cancel_reason = getValue($dom->variables->cc_cancel_reason);
    $queue_log = "INSERT INTO queue_log(
            b_lang_uuid, direction, cc_queue, cc_side, cc_member_uuid, cc_queue_joined_epoch, 
            cc_agent_bridged, cc_agent_uuid, cc_agent, cc_queue_answered_epoch, 
            cc_queue_canceled_epoch, cc_cause, cc_cancel_reason)
    VALUES ('$b_lang_uuid', '$direction', '$cc_queue', '$cc_side', '$cc_member_uuid', '$cc_queue_joined_epoch', '$cc_agent_bridged', '$cc_agent_uuid', '$cc_agent', '$cc_queue_answered_epoch', '$cc_queue_canceled_epoch', '$cc_cause', '$cc_cancel_reason')";

    $sql->SQL($queue_log);
}

