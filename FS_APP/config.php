<?php

/**
 *
 *      PHP is an Opensource App autoload configs
 *
 * 	@author Samuel da Silva Mendes
 *
 * 	Copyright (c) 2018 "App autoload configs"
 *
 * 	This file is part App autoload configs.
 *
 * 	URA App Asterisk is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * 	GNU General Public License for more details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program. If not, see <http://www.gnu.org/licenses/>
 */

/**
 * Função para escrever no syslog direto no sistemas para debug d aplicação
 * @param $dados = Stirng.
 * @param $type = Stirng
 */

function SetSyslog($dados, $type = NULL) {

    if (isset($type) == NULL) {
        openlog("SYS_CONF [LOG_INFO]", 0, LOG_LOCAL0);
        syslog(LOG_INFO, $dados);
    } else {
        $param = "FS [" . $type . "]";
        switch ($type) {
            case "EMERG":
                openlog($param, 0, LOG_LOCAL0);
                syslog(LOG_EMERG, $dados);
                break;
            case "ALERT":
                openlog($param, 0, LOG_LOCAL0);
                syslog(LOG_ALERT, $dados);
                break;
            case "CRIT":
                openlog($param, 0, LOG_LOCAL0);
                syslog(LOG_CRIT, $dados);
                break;
            case "ERR":
                openlog($param, 0, LOG_LOCAL0);
                syslog(LOG_ERR, $dados);
                break;
            case "WARNING":
                openlog($param, 0, LOG_LOCAL0);
                syslog(LOG_WARNING, $dados);
                break;
            case "NOTICE":
                openlog($param, 0, LOG_LOCAL0);
                syslog(LOG_NOTICE, $dados);
                break;
            default:
                openlog($param, 0, LOG_LOCAL0);
                syslog(LOG_DEBUG, $dados);
                break;
        }
    }
}

/**
 * Carrega o arquivo de configuração.
 * Gerando chave e valor para para o Array $conf 
 */

define('CONFIG', __DIR__ . "/ini/config.cfg");
if (!file_exists(CONFIG)) {
    $date = new DateTime ();
    $msg = $date->format('H:i:s') . "-> " . "NAO FOI POSSSIVEL ENCONTRAR O ARQUIVO DE CONFIGURACAO " . CONFIG;
    SetSyslog($msg, "WARNING");
    exit();
} else {
    $conf = parse_ini_file(__DIR__ . "/ini/config.cfg", TRUE);
}

foreach ($conf as $key => $value) {
    foreach ($value as $key_val => $value_conf) {
        if (!empty($conf['GLOBAL']['TIMEZONE']) != NULL) {
            $timezone = $conf['GLOBAL']['TIMEZONE'];
        }
        if (!empty($conf['GLOBAL']['TIMEOUT']) != NULL) {
            $timeout = $conf['GLOBAL']['TIMEOUT'];
        }
        define($key . "_" . $key_val, $conf [$key][$key_val]);
        //descomente para debug dos DEFINES
        //echo "define(" . $key . "_" . $key_val . "," . $conf [$key][$key_val] . ")" . "\n";
    }
}

/**
 * AutoLoad estrutura de diretorio.
 * Adicionar o diretorio no Array $patch
 */

spl_autoload_register(function ($class) {
    $file_dir = dirname(__FILE__) . DIRECTORY_SEPARATOR;
    $file = str_replace('\\', '/', $class . '.class.php');

    $path = $file_dir . $file;
    if (file_exists($path)) {
        require_once($path);
    } else {
        SetSyslog("File path '${path}' not found.", "ERR");
        throw new Exception("File path '${path}' not found.");
    }
});

/**
 * Setando as configuracões gerais.
 */
error_reporting(E_ALL & ~ E_NOTICE);
date_default_timezone_set($timezone);


if (!empty($timeout) != NULL) {
    // set default timeout GLOBAL PARAMS
    set_time_limit($timeout);
} else {
    // set default timeout 30s
    set_time_limit(30);
}
ob_implicit_flush(true);

/**
 * carregar as lib do NuSOAP
 * Esta fora do autoload por apresentar incompatibilidade.
 */
require_once('Lib/nusoap.php');
